# scratchpad

Random snippets of code. Think of it as gist. 

# License

Since everything we bring into this world is annoyingly copyrighted, everything in this repo is under CC0. If you live in a country that doesn't recognize CC0, then just MIT, BSD, or your favorite permissiable license. 

(Or just take it, I don't really care.)