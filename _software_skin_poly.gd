# Loose port from:
# https://github.com/godotengine/godot/blob/3.5/drivers/gles_common/rasterizer_canvas_batcher.h#L1801

onready var skeleton : Skeleton2D

func _software_skin_poly() -> void:
	var rid = skeleton.get_skeleton()
	var bone_count = VisualServer.skeleton_get_bone_count(rid)
	var skel_trans_inv = skeleton.transform.affine_inverse()

	var bone_transforms = []
	bone_transforms.resize(bone_count)
	for i in range(bone_count):
		bone_transforms[i] = VisualServer.skeleton_bone_get_transform_2d(rid, i)

	var item_transform = skel_trans_inv * poly.transform
	var item_transform_inv = item_transform.affine_inverse()

	for n in range(len(verts)):
		var src_pos = verts[n]
		var dst_pos = Vector2.ZERO
		var total_weight = 0.0
		
		var inv_src_pos = item_transform.xform(src_pos)

		for bone_id in range(len(bone_transforms)):

			var trans = bone_transforms[bone_id]
			var weight = weights[bone_id][n]
			var pos = trans.xform(inv_src_pos)

			total_weight += weight 
			dst_pos += pos * weight
	
		var final_pos = item_transform_inv.xform(dst_pos) / total_weight
		draw_circle(final_pos, 1, Color.orangered)
		draw_string(default_font, final_pos, str(n), [Color.red, Color.green][n % 2])