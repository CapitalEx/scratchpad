
#define TRIE_CAPSITY 0x8000
#define TRIE_NIL TRIE_CAPSITY
#define NON_RULE 0xffff


typedef struct {
    char token;
    unsigned short rule_id, sibling, child;
} TrieNode;


static int trie_size = 0;
static TrieNode trie[TRIE_CAPSITY];


TrieNode *add_child(TrieNode *parent, char c) {
    parent->child = trie_size++;
    TrieNode *child = trie + parent->child;
    child->token = c;
    child->sibling = child->child = TRIE_NIL;
	child->rule_id = NON_RULE;
    return child;
}


TrieNode *add_sibling(TrieNode *sib, char c) {
    sib->sibling = trie_size++;
    TrieNode *sibling = trie + sib->sibling;
    sibling->token = c;
    sibling->sibling = sibling->child = TRIE_NIL;
	sibling->rule_id = NON_RULE;
    return sibling;
}


void add_node(TrieNode *trie, char *str, unsigned short id) { 
    char c;
    TrieNode *node = trie;
    while (node->token) {
        if (*str == node->token) {
            str++; if (*str == '\0') { node->rule_id = id; return; }
            if (node->child != TRIE_NIL) { node = trie + node->child; } 
            else { node = add_child(node, *str); } 
        } else {
            if (node->sibling != TRIE_NIL) { node = trie + node->sibling; }
            else { node = add_sibling(node, *str); }
        }
    }
    while ((c = *str++)) { node = add_child(node, c); }
    node->rule_id = id;
}


void show_trie(TrieNode *node, int len, int sibling) {
    if (!node->token) return;
    for (int i = 0; sibling && i < len; i++) { putchar(' '); } putchar(node->token);
    if (node->child != TRIE_NIL) {
        if (node->rule_id != NON_RULE) { putchar('*'); }
        show_trie(trie + node->child, len + 1, 0);
    }
    if (node->sibling != TRIE_NIL) { putchar('\n'); show_trie(trie + node->sibling, len, 1); }
}
