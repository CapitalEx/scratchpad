## Crystal Code
```cr
require "llvm"
LLVM.init_x86
ctx = LLVM::Context.new


mod = ctx.new_module("main_mod")
mod.functions.add "printf", [ctx.int8.pointer] of LLVM::Type, ctx.int32, varargs: true

hello_world = mod.functions.add("hello_world", [ctx.int32] of LLVM::Type, ctx.void)
hello_world.basic_blocks.append("entrypoint") do |b|
  _0 = b.global_string_pointer("Hello, World! %d\n")
  _1 = hello_world.params[0]
  b.call mod.functions["printf"], [_0, _1]
  b.ret
end

jit = LLVM::JITCompiler.new(mod)
func_ptr = jit.get_pointer_to_global(hello_world)
func_proc = Proc(Int32, Int32).new(func_ptr, Pointer(Void).null)
func_proc.call(42)
```

## Generate LLVM IR
```llvm
; ModuleID = 'main_mod'
source_filename = "main_mod"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"

@0 = private unnamed_addr constant [18 x i8] c"Hello, World! %d\0A\00", align 1

declare i32 @printf(i8* %0, ...) #0

define void @hello_world(i32 %0) #0 {
entrypoint:
  %1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([18 x i8], [18 x i8]* @0, i32 0, i32 0), i32 %0)
  ret void
}

attributes #0 = { "frame-pointer"="none" }
```