func stateToString(state) string:
    match state:
        #cellX:
            return "X"
        #cellO:
            return "O"
        #cellEmpty:
            return "_"    

func stateToPlayer(state):
    match state:
        #cellX: return #playerX
        #cellO: return #playerO

func makeGrid() List:
    var grid List = []
    for 0..3:
        var row List = []
        for 0..3:
            row.append(#cellEmpty)
        
        grid.append(row)

    return grid

-- Mostly to make `checkForWinner()` cleaner
var checks: [
    [[0,0], [1,0], [2,0]],
    [[0,1], [1,1], [2,1]],
    [[0,2], [1,2], [2,2]],
    
    [[0,0], [0,1], [0,2]],
    [[1,0], [1,1], [1,2]],
    [[2,0], [2,1], [2,2]],

    [[0,0], [1,1], [2,2]],

    [[0,2], [1,1], [2,0]]
]

type Game object:
    grid List
    turn

    func checkForWinner(self):
        for checks each line:
            var winner = none

            for line each pair:
                var row int = pair[0]
                var col int = pair[1]

                if not winner:
                    winner = self.grid[row][col]

                else winner != self.grid[row][col]:
                    winner = none
                    break
            
            if winner and winner != #cellEmpty:
                return winner
        
        return none
    
    func readTurn(self) List:
        while true:
            var result List = getInput().utf8().split(',')
            
            if result.len() != 2:
                print("Turn must be formated as row,col.")
                continue
            
            var x int = int(result[0])
            var y int = int(result[1])

            if x > 3 or y > 3 or x < 1 or y < 1:
                print("Turn is out of bounds. Enter a pair of numbers from 1 to 3.")
                continue
            
            var turn List = [x - 1, y - 1]

            if self.grid[turn[0]][turn[1]] != #cellEmpty:
                print("Square is occupided.")
                continue
            
            return turn

    func round(self):
        print('Turn for {self.turn} (row,col):')
        var input = self.readTurn()

        var row int = input[0]
        var col int = input[1]

        self.grid[row][col] = 
            if self.turn == #playerX then #cellX else #cellO
        
        self.turn =
            if self.turn == #playerX then #playerO else #playerX

    func renderGrid(self):
        print("\n    1   2   3  ")
        for 0..3 each rowi:
            prints("{rowi+1}  ")
            var row List = self.grid[rowi]

            for 0..3 each coli:
                var col List = row[coli]
                prints(" {stateToString(col)}  ")
            
            print("\n")

var game = Game{ grid: makeGrid(), turn: #playerO }
  
while not game.checkForWinner():
    game.renderGrid()
    game.round()

game.renderGrid()
print("The winner is {stateToPlayer(game.checkForWinner())}!")