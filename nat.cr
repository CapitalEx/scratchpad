record Z do
  def to_i 
    0 
  end
end

class S(N)
  property pred : N
  def initialize(@pred : N)
  end

  macro comptime_to_i
    {%
      vars = @type.type_vars 
    %}
    {{  
      vars.reduce 0 { |i, var|
        var.type_vars.each { |inner| vars << inner }
        i + 1
      }.id
    }}
  end

  def to_i
    comptime_to_i
  end
end


def zero
  Z.new
end

def succ(s : S(N) | Z) forall N 
  S.new s
end

def pred(s : S(N)) forall N
  s.pred
end

def add(n : Z, m : S(M)) forall M
  m
end

def add(n : S(N), m : S(M)) forall N, M 
  succ add n.pred, m
end

def mul(n : Z, m : S(M)) forall M
  zero
end

def mul(n : S(N), m : Z) forall N
  zero
end

def mul(n : S(N), m : S(M)) forall N, M
  add(mul(n.pred, m), m)
end

macro i_to_nat(n)
  {% for i in (1..n) %}succ {% end %} zero
end

_1   = succ zero
_2   = succ _1
_4   = succ succ _2
_8   = add(_4, _4)
_16  = add(_8, _8)
_32  = mul(_8, _4)
_64  = mul(_8, _8)
_MAX = i_to_nat(300)