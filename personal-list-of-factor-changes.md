# Personal List of Factor Changes That Require a Focused Effort to Implement
I wanted a concise and focused list of things I or other contributers have noted
needed to be improved, implemented, or updated. Factor already has many of these
list, but I always feel they are a hodgepodge of random thoughts and additions.
Instead, I wanted a list that is focused on The Hard Things. Task that it would
take one or more people, with dedicated focus on implementing, to do.

## Core Changes
This section list changes that require re-implementation of internal details 
and/or changes to the compiler and other core libraries. These will require
deep understanding of Factor, language design, and compilers:

* [ ]  New parser
* [ ]  Efficient exceptions with stack unwinding instead of callcc
* [ ]  Compiler support for full-width integers for crypto/speed
* [ ]  Make top-level code compile/work with the non-optimizing compiler, e.g. `disable-optimizer load-all` should work
* [ ]  Multi-threading 
* [ ]  Mulit-methods

## Important API Updates/Features
This section list APIs that should be update and/or implemented. Either the
existing implementation is well-out-of-date or are a missing standard that is
widely used:

* [ ] Tools
    * [ ] Language Server
    * [ ] FFI generator with libclang
* [ ] Graphical Tech
    * [ ] GTK2 -> GTK4
    * [ ] Vulkan Bindings
    * [ ] Updated OpenGL bindings
    * [ ] Retina Support
        * [ ] MacOS
        * [ ] Linux (will very)
        * [ ] Windows
* Web
    * Furnace
        * [ ] Chloe - XHTML -> HTML5
        * [ ] gzip support
        * [ ] Support ACME
    * Tech
        * [ ] HTTP/2
        * [ ] Websockets
        * [ ] HTML5 spec parser

## Build Tagerts
A list of various build targets Factor could support. Implementing these will also 
require a deep understanding of how to compile Factor.

* [ ] Android
   * [ ] ARM support in general
* [ ] \*BSD
* [ ] WebAssembly
* [ ] iOS