# Compiling a Word in Factor
Starts at `compile-word`:
```factor
USING: combinators continuations kernel ;
IN: compiler
: compile-word ( word -- )
    [
        {
            [ start-compilation ]
            [ frontend ]
            [ backend ]
            [ finish-compilation ]
        } cleave
    ] curry with-return ;
```


# Compiler Front End
Front end handles generating the SSA IR tree that factor then uses for code generation. 
The following optimization and checks happen in the `frontend`:
- Constant propgation 
    - This also inserts whatever type information Factor can deduce
- `cleanup-tree` and `cleanup-tree*` implements
    - Unreachable code elimination 
    - Word inlining and folding
    - Overflow check elimination
    - Phi block elimination 
    - Recusive call flattening (?)
- Escape analysis
- Tuple Unboxing
- Applying arthematic identities for integers
- Def/use chain construction (?)
- Dead code elimination
- Optimizations for modular arthematic
- `finalize` (?)
    - https://docs.factorcode.org/content/article-compiler.tree.finalization.html
        - > This is a late-stage optimization. See the vocab compiler.tree.late-optimizations.
          >
          > This pass runs after propagation, so that it can expand type predicates; these cannot be expanded before propagation since we need to see 'fixnum?' instead of 'tag 0 eq?' and so on, for semantic reasoning.
          >
          > We also delete empty stack shuffles and copies to facilitate tail call optimization in the code generator.
    - https://docs.factorcode.org/content/article-compiler.tree.late-optimizations.html
        - > Late optimizations modify the tree such that stack flow information is no longer accurate, since we punt in splice-quot and don't update everything that we should; this simplifies the code, improves performance, and we don't need the stack flow information after this pass anyway.
            - What is stack flow informaiton?



## Macro Expansion 
When a macro is encountered, a literal gets read from the SSA IR tree.
This literal is then passed the macro which generates a new quotation.
That factor then generates the SSA IR for.

- `apply-macro` is envoked inside of `non-inline-word` from `M\ word apply-object`
    - calls `(apply-transform)`
         - calls `apply-literal-values-transform`