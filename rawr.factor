! Copyright (C) 2023 Capital Ex
! See http://factorcode.org/license.txt for BSD license.

USING: accessors arrays colors fonts io kernel math.trig opengl
opengl.gl quotations sequences sets ui ui.gadgets
ui.render ui.text uuid words ;
IN: rawr

! The scene-tree holds the root node and top-level timers used
! to drive the game
TUPLE: node
    uuid
    name
    children
    action ;

: init-node ( node name -- node' )
    >>name  V{ } clone >>children uuid1 >>uuid ;

: <node> ( -- node )
    node new "node" init-node ;

: (nodes) ( acc root -- )
    [ swap push ] 2keep children>> [ (nodes) ] with each ;

: nodes* ( root -- seq )
    [ V{ } clone dup ] dip (nodes) ;

: nodes ( root -- seq )
    [ V{ } clone dup ] dip (nodes) members ;

: leafs ( root -- leafs )
    nodes [ children>> empty? ] filter ;



! Generic word for converting some object into a node
GENERIC: >node ( obj -- node )

M: callable >node ( callable -- node ) call( -- node ) ;
M: word     >node ( word     -- node ) execute( -- node ) ;
M: node     >node ( node     -- node ) ;



! Generic word for adding nodes to a root.
GENERIC: add ( root node -- object )

! node#add inserts a node at every leaf in the graph.
M: node add ( root node -- root )
    swap [ leafs [ children>> push ] with each ] keep ;

! sequence#add inserts all nodes at every leaf in the graph.
M: sequence add ( root nodes -- root )
    [ dup leafs ] [ [ >node ] map ] bi* [ 2array ] with map 
    [ first2 '[ _ swap children>> push ] each ] each ;


GENERIC:  render ( node -- )
: render-children ( node -- ) children>> [ render ] each ;
M: object render ( obj  -- ) drop ;
M: node   render ( node -- ) render-children ;



TUPLE: translate < node position ;
: <translate> ( v -- translate )
    \ translate new swap >>position "translate" init-node ;

M: translate render ( node -- )
    [
        [ position>> gl-translate ] [ render-children ] bi
    ] do-matrix ;



TUPLE: rotate < node angle    ;
: <rotate> ( degrees -- rotate )
    \ rotate new swap >>angle "rotate" init-node ;

M: rotate render ( node -- )
    [ 
        [ angle>> 0 0 1 glRotated ] [ render-children ] bi
    
    ] do-matrix ;

TUPLE: scale < node factor   ;
: <scale> ( n -- scale )
    \ scale new swap >>factor "scale" init-node ;

M: scale render ( node -- )
    [ 
        [ factor>> dup 0.0 glScaled ] [ render-children ] bi
    ] do-matrix ;



TUPLE: text < node text     ;
: <text> ( string -- text )
    \ text new swap >>text "text" init-node ; 

M: text render ( node -- )
    text>> [ monospace-font ] dip draw-text ;




TUPLE: rawr-gadget < gadget tree ;
: <rawr-gadget> ( tree -- rawr-gadget )
    rawr-gadget new swap >>tree ; 

M: rawr-gadget draw-gadget* ( gadget -- )
    COLOR: black gl-color tree>> render ;

M: rawr-gadget pref-dim* ( gadget -- dim )
    drop { 800 600 } ;

: node-0 ( -- node ) <node> "node-0" >>name ;
: node-1 ( -- node ) <node> "node-1" >>name ;
: node-2 ( -- node ) <node> "node-2" >>name ;
: node-3 ( -- node ) <node> "node-3" >>name ;
: node-4 ( -- node ) <node> "node-4" >>name ;
: node-5 ( -- node ) <node> "node-5" >>name ;
: node-6 ( -- node ) <node> "node-6" >>name ;
: node-7 ( -- node ) <node> "node-7" >>name ;
: node-8 ( -- node ) <node> "node-8" >>name ;
: node-9 ( -- node ) <node> "node-9" >>name ;
: node-a ( -- node ) <node> "node-a" >>name ;
: node-b ( -- node ) <node> "node-b" >>name ;
: node-c ( -- node ) <node> "node-c" >>name ;
: node-e ( -- node ) <node> "node-e" >>name ;
: node-f ( -- node ) <node> "node-f" >>name ;


! MAIN: [
!     node-0
!     node-1 add 
!     { 
!         [ node-2 node-3 add ]
!         [ node-4 { node-5 node-6 node-7 } add ] 
!     } add
!     node-8 add
!     node-9 add nodes [ name>> print ] each
! ]

MAIN-WINDOW: rawr-test { { title "Rawr Test" } }
    COLOR: black gl-color
    COLOR: white gl-clear-color 
    <node> {
        [ 2 <scale> 
            { 200 200 } <translate> add ]
        [ { 200 200 } <translate> 
            45 <rotate> add ]
    } add "Hello, World!" <text> add <rawr-gadget> >>gadgets ;