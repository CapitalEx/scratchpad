#include <stdio.h>

#define TRIE_CAPSITY 0x8000
#define TRIE_NIL TRIE_CAPSITY
#define NO_RULE 0xffff

typedef struct {
    char token;
    unsigned short rule_index, sibling, child;
} TrieNode;

typedef struct {
	int id, refs;
	char *a, *b;
} Rule;

static int flip, quiet, rmin = 0xff, rmax = 0x00, cycles = 0x10000;
static Rule rules[0x1000], lambda, *rules_ = rules;
static char dict[0x8000], *dict_ = dict;
static char bank_a[0x4000], *src_ = bank_a;
static char bank_b[0x4000], *dst_ = bank_b;
static char *regs[0x100];

#define spacer(c) (c <= ' ' || c == '(' || c == ')')
#define special(c) (c <= ' ' || (':' <= c && c <= '@') || ('[' <= c && c <= '`') || '{' <= c)

static char *
walk(char *s)
{
	char c;
	int depth = 0;
	if(*s == '(') {
		while((c = *s++)) {
			if(c == '(') depth++;
			if(c == ')') --depth;
			if(!depth) return s;
		}
	}
	while((c = *s) && !spacer(c)) s++;
	return s;
}

static int trie_size = 0;
static TrieNode trie[TRIE_CAPSITY];

TrieNode *set_link(unsigned short *link, char c) {
   	*link = trie_size++;
    TrieNode *child = trie + *link;
    child->token = c;
    child->sibling = child->child = TRIE_NIL;
	child->rule_index = NO_RULE;
    return child;
}

static void 
show_trie(TrieNode *node, int len, int sibling) {
    int i;
    if (!node->token) return;
	for(i = 0; sibling && i < len; i++) { putchar(' '); } putchar(node->token);
    if (node->child != TRIE_NIL) {
        if (node->rule_index != NO_RULE) { putchar('*'); }
        show_trie(trie + node->child, len + 1, 0);
    }
    if (node->sibling != TRIE_NIL) { putchar('\n'); show_trie(trie + node->sibling, len, 1); }
}

static void
put_reg(char r, char *reg)
{
	char c, *cap = walk(reg);
	if(r == '*') {
		int i, depth = 0;
		if(*reg == '(') { /* special explode tuple */
			reg++;
			while(reg < cap) {
				while((c = *reg) && !spacer(c))
					*dst_++ = c, reg++;
				*dst_++ = ' ';
				*dst_++ = '(', reg++, depth++;
			}
		} else { /* special explode token */
			while((c = *reg++) && !spacer(c))
				*dst_++ = c, *dst_++ = ' ', *dst_++ = '(', depth++;
		}
		for(i = 0; i < depth; i++)
			*dst_++ = ')';
	} else if(r == '.') { /* special unpack */
		if(*reg == '(') reg++, --cap;
		while(reg < cap) *dst_++ = *reg++;
	} else if(r == '^') { /* special join */
		if(*reg == '(') reg++, --cap;
		while(reg < cap && (c = *reg++))
			if(!spacer(c)) *dst_++ = c;
	} else if(r == '~') { /* special stdin */
		while(fread(&c, 1, 1, stdin) && c >= ' ')
			*dst_++ = c;
	} else if(r == ':') { /* special stdout */
		if(*reg == '(') reg++, --cap;
		while(reg < cap) {
			c = *reg++;
			if(c == '\\') {
				switch(*reg++) {
				case 't': putc(0x09, stdout); break;
				case 'n': putc(0x0a, stdout); break;
				case 's': putc(0x20, stdout); break;
				}
			} else
				putc(c, stdout);
		}
	} else
		while(reg < cap) *dst_++ = *reg++;
}

static void 
add_node(TrieNode *trie, char *str, unsigned short id) { 
    char c;
    TrieNode *node = trie;
    while (node->token) {
        if (*str == node->token) {
            str++; if (*str == '\0') { node->rule_index = id; return; }
            if (node->child != TRIE_NIL) { node = trie + node->child; } 
            else { node = set_link(&node->child, *str); } 
        } else {
            if (node->sibling != TRIE_NIL) { node = trie + node->sibling; }
            else { node = set_link(&node->sibling, *str); }
        }
    }
    while ((c = *str++)) { node = set_link(&node->child, c); }
    node->rule_index = id;
}

unsigned short
find_rule(char *p, char **res) 
{
	char c;
	TrieNode *node = trie;
	while ((c = node->token)) {
		if (*p == c) {
			*res = ++p;
			if (*p == '\0') return node->rule_index;
			if (*p == ' ' && node->rule_index != NO_RULE) return node->rule_index;
			node = trie + node->child; continue;
		} else {
			return NO_RULE;
		}
	}
	
	return NO_RULE;
}

static char *
match_rule(Rule *r, char *p)
{
	int i;
	char c, *a = r->a;
	if(rmax) {
		for(i = rmin; i <= rmax; i++)
			regs[i] = 0;
		rmin = 0xff, rmax = 0x00;
	}
	while((c = *a)) {
		if(c == '?') {
			int regid = (int)*(++a);
			char *pcap = walk(p), *reg = regs[regid];
			if(reg) { /* reg cmp */
				char *rcap = walk(reg), *pp = p;
				while(reg < rcap || pp < pcap)
					if(*reg++ != *pp++) return NULL;
			} else { /* reg set */
				regs[regid] = p;
				if(regid < rmin) rmin = regid;
				if(regid > rmax) rmax = regid;
			}
			a++, p = pcap;
			if(!spacer(*a))
				while((c = *a) && !spacer(c)) a++;
			continue;
		}
		if(c != *p) return NULL;
		a++, p++;
	}
	c = *p;
	return spacer(c) ? p : NULL;
}

static int
commit_rule(Rule *r, char *s, int create)
{
	while((*dst_++ = *s++))
		;
	*dst_++ = 0;
	if((flip = !flip))
		src_ = bank_b, dst_ = bank_a;
	else
		src_ = bank_a, dst_ = bank_b;
	if(!quiet) {
		if(create)
			fprintf(stderr, "<> (%s) (%s)\n", r->a, r->b);
		else
			fprintf(stderr, "%02d %s\n", r->id, src_), ++r->refs;
	}
	return 1;
}

static int
write_rule(Rule *r, char *s)
{
	char c, *b = r->b, *reg, *origin = dst_;
	while((c = *b++))
		if(c == '?' && (reg = regs[(int)*b]))
			put_reg(*b++, reg);
		else
			*dst_++ = c;
	if(dst_ == origin) {
		while(*s == ' ') s++;
		if(*s == ')' && *(dst_ - 1) == ' ') dst_--;
	}
	return commit_rule(r, s, 0);
}

static char *
parse_frag(char *s, int trim)
{
	char c, *cap;
	while((c = *s) && c == ' ') s++;
	if(c != ')' && !(c == '<' && s[1] == '>')) {
		cap = walk(s);
		if(c == '(') {
			s++;
			while(s < cap - 1) {
				if (*s == '?' && trim) {
					*dict_++ = *s++, *dict_++ = *s++;
					s = walk(s);
				} else
					*dict_++ = *s++;
			}
			s++;
		} else
			while(s < cap) *dict_++ = *s++;
	}
	*dict_++ = 0;
	return s;
}

void
normalize_rule(Rule *r) {
	int i, cur = 0;
	char c, last = 0, *a = r->a, *b = r->b;
	char *name = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	char used[0x100] = {0};
	while ((c = *a)) {
		if(c == '?' && !used[c] && !special(a[1])) {
			c = *++a; *a = name[cur]; used[c] = name[cur]; cur++; walk(a);
			continue;
		}
		++a; last = c;
	}
	last = 0;
	while ((c = *b)) {
		if(c == '?' && used[b[1]] && !special(b[1])) {
			c = *++b; *b = used[c]; walk(b);
			continue;
		}
		++b; last = c;
	}
}

static int
rewrite(void)
{
	int rule_index = NO_RULE;
	char c, last = 0, *cap, *s = src_, *res;
	while(*s == ' ') s++;
	while((c = *s)) {
		if(spacer(last)) {
			Rule *r;
			if(c == '<' && s[1] == '>') {
				r = rules_++, r->id = rules_ - rules - 1;
				r->a = dict_, s = parse_frag(s + 2, 1);
				r->b = dict_, s = parse_frag(s, 0);
				while(*s == ' ') s++;
				normalize_rule(r);
				add_node(trie, r->a, r->id);
				return commit_rule(r, s, 1);
			}
			if(c == '?' && s[1] == '(') {
				cap = walk(s + 1);
				r = &lambda, r->id = -1;
				r->a = dict_, s = parse_frag(s + 2, 1);
				r->b = dict_, parse_frag(s, 0), s = cap;
				while(*s == ' ') s++;
				if((res = match_rule(&lambda, s)) != NULL)
					return write_rule(&lambda, res);
			}
			if ((rule_index = find_rule(s, &res)) != NO_RULE)
				return write_rule(rules + rule_index, res);
			/*
for(r = rules; r < rules_; r++)
	if((res = match_rule(r, s)) != NULL)
		return write_rule(r, res);
			*/
			
		}
		*dst_++ = last = c;
		s++;
	}
	*dst_++ = 0;
	return 0;
}

int
main(int argc, char **argv)
{
	FILE *f;
	int i, pl = 0, pr = 0;
	char c, *w = bank_a;
	if(argc < 2)
		return !printf("usage: modal [-vqn] source.modal\n");
	for(i = 1; i < argc && *argv[i] == '-'; i++) {
		switch(argv[i][1]) {
		case 'v': /* version */ return !printf("Modal Interpreter, 22 Apr 2024.\n");
		case 'q': /* quiet */ quiet = 1; break;
		case 'n': /* infinite */ cycles = 0xffffffff; break;
		}
	}
	if(!(f = fopen(argv[i], "r")))
		return !fprintf(stdout, "Modal file invalid: %s.\n", argv[i]);
	while(fread(&c, 1, 1, f)) {
		c = c <= 0x20 ? 0x20 : c;
		if(w > bank_a) {
			if(c == ' ' && *(w - 1) == '(') continue;
			if(c == ')' && *(w - 1) == ' ') w--;
			if(c == ' ' && *(w - 1) == ' ') w--;
			if(c == '(') pl++;
			if(c == ')') pr++;
		}
		*w++ = c;
	}
	while(*(--w) <= ' ') *w = 0;
	fclose(f);
	if(pr != pl)
		return !fprintf(stdout, "Modal program imbalanced.\n");
	while(rewrite())
		if(!cycles--) return !fprintf(stdout, "Modal rewrites exceeded.\n");
	while(rules_-- > rules && !quiet)
		if(!rules_->refs) printf("-- Unused rule: %d <> (%s) (%s)\n", rules_->refs, rules_->a, rules_->b);
	show_trie(trie, 0, 0);
	putchar('\n');
	return 0;
}