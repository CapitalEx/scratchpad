module A end 
module B end
module C end

struct X ; include A ; end
struct Y ; include B ; end 
struct Z ; include A ; include B ; include C ; end

xyz = [X.new, Y.new, Z.new]
def f1(xs : Array(A)) end
def f2(xs : Array(B)) end
def f3(xs : Array(C)) end
a_list = xyz.select A
b_list = xyz.select B
c_list = xyz.select C

f1 a_list
f2 b_list
f3 c_list

# While all of the above is fine, the following line will fail:
#   abc = a_list | b_list | c_list
#
# In the following error:
#   In /usr/lib/crystal/array.cr:320:16
#   
#    320 | hash[obj] = true
#               ^--
#   Error: expected argument #1 to 'Hash(A, Bool)#[]=' to be A, not B
#   
#   Overloads are:
#    - Hash(K, V)#[]=(key : K, value : V)

def g(xs : Array(A | B)) end
ab_list = xyz.select A | B

#
# Attemping to call g as followed:
#   g(ab_list)
#
# Will result in the follow error message:
#   In src/types-playing.cr:39:3
#   
#    39 | g(ab_list)
#           ^------
#   Error: expected argument #1 to 'g' to be Array(A | B), not Array(A | B)
#   
#   Overloads are:
#    - g(xs : Array(A | B))