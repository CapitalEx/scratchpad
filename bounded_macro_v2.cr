annotation Bounded end
macro validate_bounds!
  {% 
    @type.annotations(Bounded).each do |bound|
      bounded      = bound.args[0]
      valid_types  = bound.args[1].map(&.id)
      generic_base = bounded.resolve.id

      unless valid_types.includes? generic_base
        raise "Can't use #{generic_base.stringify} as parameter. \
              #{bounded} is bounded by #{valid_types}."
      end
    end
  %}
end

class A end
class B end 
class C end 

@[Bounded(S, {A, B})]
@[Bounded(T, {B, C})]
class BoundedPair(S, T)
  property first  : S
  property second : T
  
  def initialize(@first, @second)
    validate_bounds!
  end
end

BoundedPair.new(B.new, A.new)