# Add bounds to a generic type
annotation Bounded end
macro validate_bounds!
  {% 
    generic_base = @type.type_vars[0].name generic_args: false
    valid_types  = @type.annotation(Bounded).args.map(&.id)
    raise "\
      Can't use #{generic_base.stringify} as parameter. \
      N is bounded by #{valid_types} \
    " unless valid_types.includes? generic_base
  %}
end

# Find the intersection of multiple mixins
macro intersect(call)
  {%
    types_to_resolve = [call]
    types = [] of Nil

    types_to_resolve.each do |t|
      if t.is_a?(Call)
        types_to_resolve << t.receiver
        t.args.each { |tv| types_to_resolve << tv }
      else
        types << t.resolve
      end
    end

    set = types[0].includers

    intersect = types[1..].reduce(set) { |set, type|
      set.select { |t| type.includers.includes? t }
    }
  %}

  {{ "Union(#{intersect.splat})".id }}
end

# Find all classes that include some mixin
macro includes(call)
  {%
    types_to_resolve = [call]
    types = { } of Nil => Nil


    types_to_resolve.each do |t|
      if t.is_a?(Call)
        types_to_resolve << t.receiver
        t.args.each { |tv| types_to_resolve << tv }
      else
        t.resolve.includers.each { |ti|
          types[ti] = true
        }
      end
    end
  %}

  {{ "Union(#{types.keys.splat})".id }}
end