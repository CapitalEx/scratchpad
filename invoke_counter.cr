module InvokeCounter
    annotation InvokeCounter end
    
    struct CreatedAt(LineNumber, ColumnNumber, Id) end
    macro test!(x)
        {%
            sanitized = x.filename.downcase.gsub(/[^a-zA-Z0-9]/, "")
            id_number = sanitized.chars.shuffle[0...11].join("").to_i 36
        %}
        InvokeCounter.test({{x}}, {{"InvokeCounter::CreatedAt(#{x.line_number}, #{x.column_number}, #{id_number})".id}})
        {{debug}}
    end

    @[InvokeCounter(counter: {:value => 0})]
    def self.test(x, t)
        {% 
            @def.annotation(InvokeCounter)[:counter][:value] += 1
        %}
    
        count = {{ @def.annotation(InvokeCounter)[:counter][:value] }}

        {called: count, value: x}
    end
end

puts InvokeCounter.test!(1) # => {called: 1, value: 1}
puts InvokeCounter.test!(1) # => {called: 2, value: 1}
puts InvokeCounter.test!(1) # => {called: 3, value: 1}