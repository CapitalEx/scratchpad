annotation RequiredSequence ; end

@[RequiredSequence(
  info: {
    :state => :none,

    :none          => :lock_first,
    :lock_first    => :lock_second,
    :lock_second   => :unlock_second,
    :unlock_second => :none,
  }
)]
module PairAccessor
  abstract class Token(Id) ; end

  # Validates the transition rules of the FSM
  macro must_come_from!(expected_state)
    {%
      sm = parse_type("PairAccessor").resolve.annotation(RequiredSequence)[:info] 
      unless sm[:state] == expected_state
        raise "Expected to be in the `#{expected_state}` state, but was in `#{sm[:state]}`.\n"
      end
      puts "#{sm}"
      sm[:state] = sm[sm[:state]]
    %}
  end

  # This macro is required to force a unique implementation of resource
  # accessors. Otherwise the rules can't be validated on each call at compile
  # time.
  macro must_take_block_arg!
    {%
      unless @def.accepts_block?
        raise "Resource accessors must take block arg."
      end
    %}
    end

  # Sugar macro to help define sequenced blocks. Takes a symbol for the current 
  # state and a block.
  macro def_from(sym, &block)
    {% name = block.body.name %}
    
    macro {{name}}
      {%
        sm = parse_type("PairAccessor").resolve.annotation(RequiredSequence)[:info]
        sm[:id] += 1 
      %}
      PairAccessor.must_take_block_arg!
      PairAccessor.must_come_from! {{sym}}
      PairAccessor._{{name}}
    end

    def self._{{name}}
      {{block.body.body}}
    end
  end

  def_from :none do
    def lock_first
      puts "Locking   First  🔒"
    end
  end

  def_from :lock_first do
    def lock_second
      puts "Locking   Second 🔒"
    end
  end
  
  def_from :lock_second do
    def unlock_second
      puts "Unlocking Second 🔑"
    end 
  end

  def_from :unlock_second do
    def unlock_first
      puts "Unlocking First  🔑"
    end
  end
end

def correct(&)
  PairAccessor.lock_first
  PairAccessor.lock_second
  yield
  PairAccessor.unlock_second
  PairAccessor.unlock_first
end

def never_releases(&)
  PairAccessor.lock_first
  PairAccessor.lock_second
end

def only_releases(&)
  PairAccessor.unlock_second
  PairAccessor.unlock_first
end

def wrong_order
  PairAccessor.lock_second
  PairAccessor.lock_first

  PairAccessor.unlock_second
  PairAccessor.unlock_first
end

# never_releases {}
correct {}
